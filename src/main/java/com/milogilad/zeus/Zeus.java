package com.milogilad.zeus;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public final class Zeus extends JavaPlugin {
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("smite")) {

			if (!sender.hasPermission("zeus.smite")) {
				sender.sendMessage("You're not permitted to use that command!");
				return false;
			}
			
			boolean result = smite(args[0]);
			
			if (result) {
			    sender.sendMessage("Smited " + args[0]);
				return true;
			} else {
				sender.sendMessage("Player is not online or does not exist!");
				return false;
			}
			
		}

		return false;
	}
	
	private boolean smite(String username) {
		Player p = null;
		
		getLogger().info("Looking for " + username);
		
		for (Player o : Bukkit.getServer().getOnlinePlayers()) {
			getLogger().info(o.getName());
			if (o.getName().equals(username)) {
				getLogger().info("Match!");
				p = o;
				break;
			}
		}
		
		if (p == null) {
			return false;
		}
		
		p.getWorld().strikeLightning(p.getLocation());
		return true;

	}

}
