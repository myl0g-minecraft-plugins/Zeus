# Zeus

A Minecraft bukkit plugin adding a single command which can smite the given player (striking them with lightning).

## Usage

`/smite [username]`

## Permissions

zeus.smite (op-only by default)
